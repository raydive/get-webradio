# get-webradio

download audio files from some web-radio sites

## Installation

    $ git clone http://bitbucket.org/tdtds/get-webradio.git
    $ cd get-webradio
    $ rake install

## Usage

    rget imachu          # download Radio de ima-chu!!
    rget imas_cg         # download THE iDOLM@STER Cinderella Girls Radio
    rget imastudio       # download imastudio

